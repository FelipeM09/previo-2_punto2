/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.negocio;

import java.util.Iterator;
import ufps.modelo.Radical;
import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author MADARME
 */
public class SistemaRadical {

    private ListaCD<Radical> L1 = new ListaCD();
    private ListaCD<Radical> L2 = new ListaCD();

    public SistemaRadical() {
    }

    public SistemaRadical(ListaCD<Radical> L1, ListaCD<Radical> L2) {
        this.L1 = L1;
        this.L2 = L2;
    }
   
    public ListaCD<Radical> getL1() {
        return L1;
    }

    public void setL1(ListaCD<Radical> L1) {
        this.L1 = L1;
    }

    public ListaCD<Radical> getL2() {
        return L2;
    }

    public void setL2(ListaCD<Radical> L2) {
        this.L2 = L2;
    }

    public ListaCD<Radical> sumarRadicales() throws ExceptionUFPS {
        
        if(L1.esVacia() || L2.esVacia()){
            throw new ExceptionUFPS("Listas estan Vacias");
        }
        
        ListaCD<Radical> resultado = new ListaCD();
        Iterator<Radical> iterador1 = L1.iterator();
        Iterator<Radical> iterador2 = L2.iterator();
        
        while(iterador1.hasNext()){
            Radical rad = iterador1.next();            
            while(iterador2.hasNext()){
                Radical rad2 = iterador2.next();
                if(rad.equals(rad2)){
                    float coefi = (rad.getCoeficiente()+rad2.getCoeficiente());
                    Radical result = new Radical(coefi,rad2.getIndice(),rad2.getRadicando());                   
                    resultado.insertarOrdenado(result);
                }               
            }
            iterador2 = L2.iterator();
        }
        
          encontrarL1(resultado);
          encontrarL2(resultado);
    
        return resultado;
    }
    
    private void encontrarL1(ListaCD<Radical> resultado) {
        Iterator<Radical> iterador1 = L1.iterator();
     

       while (iterador1.hasNext()) {
            Radical rad1 = iterador1.next();
                if(!seEnCuentraEn(resultado, rad1))
                    resultado.insertarOrdenado(rad1);
                }

    }

    private void encontrarL2(ListaCD<Radical> resultado) {
        Iterator<Radical> iterador1 = L2.iterator();
      

        while (iterador1.hasNext()) {
            Radical rad1 = iterador1.next();
                if(!seEnCuentraEn(resultado, rad1))
                    resultado.insertarOrdenado(rad1);
                }
           
        
    }
    private Boolean seEnCuentraEn(ListaCD<Radical> l1,Radical r){
    
        for(Radical r2:l1){
            if(r2.equals(r))
                return true;
        }
    
        return false;
    }
      
  
}
