/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.modelo;

/**
 *
 * @author MADARME
 */
public class Radical implements Comparable<Radical> {
    
    private float coeficiente;
    private int indice;
    private float radicando;

    public Radical() {
    }

    public Radical(float coeficiente, int indice, float radicando) {
        this.coeficiente = coeficiente;
        this.indice = indice;
        this.radicando = radicando;
    }

    public float getCoeficiente() {
        return coeficiente;
    }

    public void setCoeficiente(float coeficiente) {
        this.coeficiente = coeficiente;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public float getRadicando() {
        return radicando;
    }

    public void setRadicando(float radicando) {
        this.radicando = radicando;
    }

    @Override
    public String toString() {
        return this.coeficiente+" "+this.indice+"√("+this.radicando+")";
    }

  

    @Override
    public int compareTo(Radical t) {
        return (int)(this.getIndice()-t.getIndice());
    }
    
      

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.indice;
        hash = 53 * hash + Float.floatToIntBits(this.radicando);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Radical other = (Radical) obj;
        if (this.indice == other.indice && this.radicando == other.radicando) {
            return true;
        }        
        return false;
    }
    
    
    
    
}
