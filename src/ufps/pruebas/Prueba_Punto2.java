/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.pruebas;

import ufps.modelo.Radical;
import ufps.negocio.SistemaRadical;
import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaCD;

/**
 * Coloque su nombre y código:
 *
 * @author Manuel Felipe Mora Espitia 1151863
 */
public class Prueba_Punto2 {

    public static void main(String[] args) throws ExceptionUFPS {
       
//        Radical radical1 = new Radical(3.0f, 2, 5.0f);
//        Radical radical2 = new Radical(-3.0f, 3, 5.0f);
//        Radical radical3 = new Radical(-4.0f, 4, 6.0f);
//        Radical radical4 = new Radical(12.0f, 4, 6.0f);
//        Radical radical5 = new Radical(1.0f, 3, 5.0f);
//        Radical radical6 = new Radical(-3.0f, 7, 4.0f);
        
        Radical radical1 = new Radical(43.0f, 98, 234.0f);
        Radical radical2 = new Radical(65.0f, 678, 987.0f);
        Radical radical3 = new Radical(-123.0f, 567, 875.0f);
        Radical radical4 = new Radical(633.0f, 98, 234.0f);
        Radical radical5 = new Radical(-98.0f, 567, 875.0f);
        
        
        
     

        ListaCD<Radical> L1 = new ListaCD<>();
        ListaCD<Radical> L2 = new ListaCD<>();
        
        L1.insertarOrdenado(radical1);
        L1.insertarOrdenado(radical2);
        L1.insertarOrdenado(radical3);
        
        L2.insertarOrdenado(radical4);
        L2.insertarOrdenado(radical5);
//        L2.insertarOrdenado(radical6);

        SistemaRadical sistema = new SistemaRadical(L1, L2);        

        try {
            System.out.println("Radicales Lista 1 = " + sistema.getL1().toString());
            System.out.println("Radicales Lista 2 = " + sistema.getL2().toString());
            System.out.println("Resultado de la Suma = " + sistema.sumarRadicales().toString());

        } catch (ExceptionUFPS e) {
            System.out.println(e.getMensaje());
        }

    }

}
