/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.negocio;

import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ufps.modelo.Radical;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author FELIPE MORA
 */
public class SistemaRadicalTest {   
    

   
    @Test
    public void testSumarRadicales() throws Exception {
        SistemaRadical sistema = new SistemaRadical();
        boolean correcto = true;
        ListaCD<Radical> lista1 = new ListaCD();
        ListaCD<Radical> lista2 = new ListaCD();
        
        Radical radical1 = new Radical(4.0f, 2, 2.0f);
        Radical radical2 = new Radical(-1.0f, 5, 8.0f);
        Radical radical3 = new Radical(-6.0f, 7, 4.0f);
        Radical radical4 = new Radical(2.0f, 2, 2.0f);
        Radical radical5 = new Radical(-5.0f, 7, 4.0f);
        
        
        lista1.insertarOrdenado(radical1);
        lista1.insertarOrdenado(radical2);
        lista1.insertarOrdenado(radical3);
        
        lista2.insertarOrdenado(radical4);
        lista2.insertarOrdenado(radical5);
        
        sistema.setL1(lista1);
        sistema.setL2(lista2);
        
        ListaCD<Radical> listaResul = new ListaCD();
        
        listaResul.insertarAlFinal(new Radical(6.0f,2,2.0f));
        listaResul.insertarAlFinal(new Radical(-1.0f,5,8.0f));
        listaResul.insertarAlFinal(new Radical(-11.0f,7,4.0f));
        
        ListaCD<Radical> nueva = sistema.sumarRadicales();
        
        Iterator<Radical> iterador = listaResul.iterator();
        
        for(Radical r: nueva){
            if(!r.equals(iterador.next())){
                correcto = false;
            }
        }
         assertTrue(correcto);
        
    }
    
     @Test
    public void testSumarRadicales2() throws Exception {
        SistemaRadical sistema = new SistemaRadical();
        boolean correcto = true;
        ListaCD<Radical> lista1 = new ListaCD();
        ListaCD<Radical> lista2 = new ListaCD();
        
        Radical radical1 = new Radical(43.0f, 98, 234.0f);
        Radical radical2 = new Radical(65.0f, 678, 987.0f);
        Radical radical3 = new Radical(-123.0f, 567, 875.0f);
        Radical radical4 = new Radical(633.0f, 98, 234.0f);
        Radical radical5 = new Radical(-98.0f, 567, 875.0f);
        
       
        
        lista1.insertarOrdenado(radical1);
        lista1.insertarOrdenado(radical2);
        lista1.insertarOrdenado(radical3);
        
        lista2.insertarOrdenado(radical4);
        lista2.insertarOrdenado(radical5);
        
        sistema.setL1(lista1);
        sistema.setL2(lista2);
        
        ListaCD<Radical> listaResul = new ListaCD();
        
        listaResul.insertarAlFinal(new Radical(676.0f, 98, 234.0f));
        listaResul.insertarAlFinal(new Radical(-221.0f,567,875.0f));
        listaResul.insertarAlFinal(new Radical(65.0f,678,987.0f));
        
        ListaCD<Radical> nueva = sistema.sumarRadicales();
        
        Iterator<Radical> iterador = listaResul.iterator();
        
        for(Radical r: nueva){
            if(!r.equals(iterador.next())){
                correcto = false;
            }
        }
         assertTrue(correcto);
        
    }
    
}
